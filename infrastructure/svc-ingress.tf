module "svc-ingress-namespace" {
  source = "./modules/namespace"
  name   = "svc-ingress"
}
resource "kubernetes_network_policy_v1" "svc-ingress-ingress" {
  metadata {
    name      = "allow-svclb-ingress"
    namespace = module.svc-ingress-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = "0.0.0.0/0"
        }
      }
      ports {
        port     = "8080"
        protocol = "TCP"
      }
      ports {
        port     = "8443"
        protocol = "TCP"
      }
    }
  }
}

resource "kubernetes_network_policy_v1" "svc-ingress-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-ingress-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "9100"
        protocol = "TCP"
      }
    }
  }
}

resource "helm_release" "svc-ingress" {
  name       = "svc-ingress"
  repository = "https://traefik.github.io/charts"
  chart      = "traefik"
  namespace  = module.svc-ingress-namespace.name

  values = ["${file("data/svc-ingress/values.yaml")}"]
}

resource "kubernetes_manifest" "tlsoptions" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "TLSOption"
    metadata = {
      name      = "user-ca"
      namespace = module.svc-ingress-namespace.name
    }
    spec = {
      minVersion       = "VersionTLS12"
      maxVersion       = "VersionTLS13"
      curvePreferences = ["CurveP521", "CurveP384"]
      cipherSuites     = ["TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_RSA_WITH_AES_256_GCM_SHA384"]
      sniStrict        = false
      clientAuth = {
        secretNames    = ["wtbt-root-ca"]
        clientAuthType = "VerifyClientCertIfGiven"
      }
    }
  }
}

resource "kubernetes_service_v1" "svc-ingress-prometheus" {
  metadata {
    name      = "prometheus"
    namespace = module.svc-ingress-namespace.name
  }
  spec {
    type          = "ExternalName"
    external_name = "prom-ip.service.wtbt.uk"
  }
}
