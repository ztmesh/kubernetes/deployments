module "app-netbox-namespace" {
  source = "./modules/namespace"
  name = "app-netbox"
}
module "app-netbox-smtp" {
  source = "./modules/smtp"

  from         = "netbox"
  from_name    = "Netbox"
  email_domain = var.ses_mail_domain
}

module "app-netbox-db" {
  source = "./modules/postgres"

  name = "netbox"
}

resource "helm_release" "app-netbox" {
  name       = "app-netbox"
  repository = "https://charts.boo.tc"
  chart      = "netbox"
  namespace  = module.app-netbox-namespace.name

  atomic  = true
  timeout = 600

  values = ["${templatefile("data/app-netbox/values.yaml", {
    postgres_host     = module.app-netbox-db.host
    postgres_database = module.app-netbox-db.database
    postgres_username = module.app-netbox-db.username
    postgres_password = module.app-netbox-db.password
    smtp_host         = module.app-netbox-smtp.host
    smtp_username     = module.app-netbox-smtp.username
    smtp_password     = module.app-netbox-smtp.password
    smtp_from         = module.app-netbox-smtp.from
    smtp_from_name    = module.app-netbox-smtp.from_name
  })}"]
}

module "app-netbox-ingress" {
  source = "./modules/ingress"

  service-name = "app-netbox"
  service-port = "80"
  domain       = "netbox.service.wtbt.uk"
  namespace    = module.app-netbox-namespace.name

  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk"
  ]
}

module "app-netbox-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-netbox-db
  namespace         = module.app-netbox-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}