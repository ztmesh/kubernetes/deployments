module "svc-s3-namespace" {
  source = "./modules/namespace"
  name = "svc-s3"
}

resource "random_password" "svc-s3-admin_token" {
  length = 20
  special = false
}

resource "helm_release" "svc-s3" {
  name       = "garage"
  repository = "https://kfirfer.github.io/charts/"
  chart      = "garage"
  namespace  = module.svc-s3-namespace.name

  values = [templatefile("data/svc-s3/values.yaml", {
    admin_token = random_password.svc-s3-admin_token.result
  })]
}

module "svc-s3" {
  source = "./modules/ingress"

  service-name = "garage-local"
  service-port = "3900"
  domain       = "s3.service.wtbt.uk"
  namespace    = module.svc-s3-namespace.name
}

resource "kubernetes_network_policy_v1" "svc-s3-cf-ingress" {
  metadata {
    name = "cloudflare-ingress"
    namespace = module.svc-s3-namespace.name
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        namespace_selector {
          match_labels = {
            namespace = module.svc-ingress-cloudflare-namespace.name
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "svc-s3-local" {
  metadata {
    name      = "garage-local"
    namespace = module.svc-s3-namespace.name
  }
  spec {
    internal_traffic_policy = "Local"
    ip_families             = ["IPv4"]
    ip_family_policy        = "SingleStack"
    port {
      name        = "s3-api"
      port        = 3900
      protocol    = "TCP"
      target_port = "3900"
    }
    port {
      name        = "s3-web"
      port        = 3902
      protocol    = "TCP"
      target_port = "3902"
    }
    selector = {
      "app.kubernetes.io/instance" = "garage"
      "app.kubernetes.io/name"     = "garage"
    }
    session_affinity = "None"
    type             = "ClusterIP"
  }
}

resource "kubernetes_network_policy_v1" "svc-s3-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-s3-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "8000"
        protocol = "TCP"
      }
      ports {
        port = "3903"
        protocol = "TCP"
      }
    }
  }
}

resource "kubernetes_deployment_v1" "svc-s3-prometheus-exporter" {
  metadata {
    name      = "s3-exporter"
    namespace = module.svc-s3-namespace.name
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "s3-exporter"
      }
    }

    template {
      metadata {
        labels = {
          app = "s3-exporter"
        }

        annotations = {
          "prometheus.io/scrape" = true
          "prometheus.io/port"   = 8000
          "prometheus.io/path"   = "/metrics"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/codingjwilliams/garagehq-stats-exporter:main"
          name  = "exporter"

          resources {
            requests = {
              cpu    = "50m"
              memory = "30Mi"
            }
          }

          env {
            name  = "GARAGE_ADMIN_ENDPOINT"
            value = "http://garage-metrics.${module.svc-s3-namespace.name}.svc.int.kubernetes.wtbt.uk:3903"
          }

          env {
            name  = "GARAGE_ADMIN_TOKEN"
            value = random_password.svc-s3-admin_token.result
          }

          port {
            name           = "metrics"
            protocol       = "TCP"
            container_port = 8000
          }
        }
      }
    }
  }
}
