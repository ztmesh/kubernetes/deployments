module "app-homehub-namespace" {
  source = "./modules/namespace"
  name   = "app-homehub"
}

resource "kubernetes_config_map_v1" "app-homehub" {
  metadata {
    name      = "homehub"
    namespace = module.app-homehub-namespace.name
  }

  data = {
    "php-fpm.conf" = file("data/app-homehub/php-fpm.conf")
    "php.ini"      = file("data/app-homehub/php.ini")
    "Caddyfile"    = file("data/app-homehub/Caddyfile")
  }
}

module "app-homehub-s3-backup" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-backup-cronjob.git?ref=v1.1.0"

  name                        = "homehub-s3"
  namespace                   = module.app-homehub-namespace.name
  rsync_private_key           = var.rsync_private_key
  rsync_username              = var.rsync_username
  cron = "19 2 * * *"
  enable_privileged_container = true
  source_configuration        = <<EOT
driver=s3
endpoint=https://s3.service.wtbt.uk
access_key_id=${var.app_homehub_s3_access_key_id}
secret_access_key=${var.app_homehub_s3_secret_access_key}
region=garage
ca_cert=/ca.crt
bucket=homehub
EOT
  extra_files = [{
    name       = "ca.crt"
    mount_path = "/ca.crt"
    content    = file("data/ca.crt")
  }]
}

resource "random_bytes" "app-homehub-key" {
  length = 32
}

resource "random_bytes" "app-homehub-key2" {
  length = 32
}

resource "kubernetes_deployment_v1" "app-homehub" {
  metadata {
    name      = "homehub"
    namespace = module.app-homehub-namespace.name

    labels = {
      k8s-app              = "homehub"
      "kubernetes.io/name" = "HomeHub"
    }
  }

  spec {
    revision_history_limit = 0
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_unavailable = 1
      }
    }

    selector {
      match_labels = {
        "k8s-app" = "homehub"
      }
    }

    template {
      metadata {
        labels = {
          "k8s-app" = "homehub"
        }
      }

      spec {
        node_selector = {
          "topology.kubernetes.io/zone" = "a"
        }

        container {
          name              = "homehub"
          image             = "registry.gitlab.com/codingjwilliams/homehub:master"
          image_pull_policy = "Always"
          resources {
            limits = {
              memory = "170Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "70Mi"
            }
          }

          volume_mount {
            name       = "config"
            mount_path = "/etc/caddy/Caddyfile"
            sub_path   = "Caddyfile"
          }

          volume_mount {
            name       = "ca"
            mount_path = "/ca.crt"
            sub_path   = "ca.crt"
          }

          volume_mount {
            name       = "config"
            mount_path = "/usr/local/etc/php-fpm.d/caddy.conf"
            sub_path   = "php-fpm.conf"
          }

          volume_mount {
            name       = "config"
            mount_path = "/usr/local/etc/php/conf.d/00-littlewonders.ini"
            sub_path   = "php.ini"
          }

          volume_mount {
            name       = "logs"
            mount_path = "/var/www/html/storage/logs/"
          }

          env {
            name  = "APP_NAME"
            value = "homehub"
          }
          env {
            name  = "APP_ENV"
            value = "production"
          }
          env {
            name  = "APP_KEY"
            value = "base64:${random_bytes.app-homehub-key.base64}"
          }
          env {
            name  = "APP_DEBUG"
            value = "false"
          }
          env {
            name  = "APP_URL"
            value = "https://homehub.service.wtbt.uk"
          }
          env {
            name  = "FRONTEND_URL"
            value = "https://homehub.service.wtbt.uk"
          }
          env {
            name  = "LOG_CHANNEL"
            value = "stack"
          }
          env {
            name  = "LOG_DEPRECATIONS_CHANNEL"
            value = "null"
          }
          env {
            name  = "LOG_LEVEL"
            value = "debug"
          }
          env {
            name  = "DB_CONNECTION"
            value = "mysql"
          }
          env {
            name  = "DB_HOST"
            value = "10.5.1.3"
          }
          env {
            name  = "DB_PORT"
            value = "3306"
          }
          env {
            name  = "DB_DATABASE"
            value = "homehub"
          }
          env {
            name  = "DB_USERNAME"
            value = "homehub"
          }
          env {
            name  = "DB_PASSWORD"
            value = var.app_homehub_mysql_password
          }
          env {
            name  = "BROADCAST_DRIVER"
            value = "log"
          }
          env {
            name  = "CACHE_DRIVER"
            value = "file"
          }
          env {
            name  = "FILESYSTEM_DISK"
            value = "s3"
          }
          env {
            name  = "QUEUE_CONNECTION"
            value = "sync"
          }
          env {
            name  = "SESSION_DRIVER"
            value = "file"
          }
          env {
            name  = "SESSION_LIFETIME"
            value = "120"
          }
          env {
            name  = "AZURE_TENANT_ID"
            value = "g8.fyi"
          }
          env {
            name  = "AZURE_REDIRECT_URI"
            value = "https://homehub.service.wtbt.uk/api/auth/callback"
          }
          env {
            name  = "AZURE_CLIENT_ID"
            value = var.app_homehub_azure_client_id
          }
          env {
            name  = "AZURE_CLIENT_SECRET"
            value = var.app_homehub_azure_client_secret
          }
          env {
            name  = "AWS_USE_PATH_STYLE_ENDPOINT"
            value = "true"
          }
          env {
            name  = "AWS_ACCESS_KEY_ID"
            value = var.app_homehub_s3_access_key_id
          }
          env {
            name  = "AWS_SECRET_ACCESS_KEY"
            value = var.app_homehub_s3_secret_access_key
          }
          env {
            name  = "AWS_ENDPOINT"
            value = "https://s3.g8.fyi"
          }
          env {
            name  = "AWS_BUCKET"
            value = "homehub"
          }
          env {
            name  = "AWS_DEFAULT_REGION"
            value = "garage"
          }
          env {
            name  = "JWT_SECRET"
            value = random_bytes.app-homehub-key.hex
          }
          env {
            name  = "JWT_ALGO"
            value = "HS256"
          }
          env {
            name  = "REACT_APP_API"
            value = "https://homehub.service.wtbt.uk"
          }

          port {
            container_port = 80
            name           = "http"
            protocol       = "TCP"
          }
        }

        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map_v1.app-homehub.metadata[0].name
          }
        }

        volume {
          name = "ca"
          secret {
            secret_name = "wtbt-root-ca"
            items {
              key  = "ca.crt"
              path = "ca.crt"
            }
          }
        }

        volume {
          name = "logs"
          empty_dir {

          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "app-homehub" {
  metadata {
    name      = "homehub"
    namespace = module.app-homehub-namespace.name
    labels = {
      "k8s-app" = "homehub"
    }
  }

  spec {
    selector = {
      "k8s-app" = "homehub"
    }

    port {
      name        = "http"
      port        = "80"
      target_port = "http"
      protocol    = "TCP"
    }
    ip_family_policy = "SingleStack"
    type             = "ClusterIP"
    session_affinity = "None"
    ip_families      = ["IPv4"]
  }
}

module "app-homehub-ingress" {
  source = "./modules/ingress"

  service-name = kubernetes_service_v1.app-homehub.metadata[0].name
  service-port = "http"
  domain       = "homehub.service.wtbt.uk"
  namespace    = module.app-homehub-namespace.name

  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk"
  ]
}

module "app-homehub-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-mysql-backup-cronjob.git?ref=v1.0.0"

  database = {
    host     = "10.5.1.3"
    database = "homehub"
    username = "homehub"
    password = var.app_homehub_mysql_password
  }
  namespace         = module.app-homehub-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}
