module "svc-gitlab-runner-namespace" {
  source = "./modules/namespace"
  name = "svc-gitlab-runner"
}

resource "helm_release" "svc-gitlab-runner" {
  for_each = { for i, v in var.gitlab_runner_tokens : i => v }

  name       = "runner-${each.key}"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace  = module.svc-gitlab-runner-namespace.name

  values = ["${file("data/svc-gitlab-runner/values.yaml")}"]

  set {
    name  = "runnerToken"
    value = each.value
  }
}


resource "kubernetes_network_policy_v1" "svc-gitlab-runner-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-gitlab-runner-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "9252"
        protocol = "TCP"
      }
    }
  }
}