module "app-nextcloud-namespace" {
  source = "./modules/namespace"
  name   = "app-nextcloud"
}
module "app-nextcloud-smtp" {
  source = "./modules/smtp"

  from         = "nextcloud"
  from_name    = "Nextcloud"
  email_domain = var.ses_mail_domain
}

module "app-nextcloud-db" {
  source = "./modules/postgres"

  name = "nextcloud"
}

resource "helm_release" "app-nextcloud" {
  name       = "app-nextcloud"
  repository = "https://nextcloud.github.io/helm/"
  chart      = "nextcloud"
  namespace  = module.app-nextcloud-namespace.name

  atomic  = true
  timeout = 600

  values = ["${templatefile("data/app-nextcloud/values.yaml", {

  })}"]

  set {
    name  = "nextcloud.mail.fromAddress"
    value = split("@", module.app-nextcloud-smtp.from)[0]
  }

  set {
    name  = "nextcloud.mail.domain"
    value = var.ses_mail_domain
  }

  set {
    name  = "nextcloud.mail.smtp.host"
    value = module.app-nextcloud-smtp.host
  }

  set {
    name  = "nextcloud.mail.smtp.name"
    value = module.app-nextcloud-smtp.username
  }

  set {
    name  = "nextcloud.mail.smtp.password"
    value = module.app-nextcloud-smtp.password
  }

  set {
    name  = "externalDatabase.host"
    value = "${module.app-nextcloud-db.host}:5432"
  }

  set {
    name  = "externalDatabase.database"
    value = module.app-nextcloud-db.database
  }

  set {
    name  = "externalDatabase.user"
    value = module.app-nextcloud-db.username
  }

  set {
    name  = "externalDatabase.password"
    value = module.app-nextcloud-db.password
  }
}

module "app-nextcloud-ingress" {
  source = "./modules/ingress"

  service-name = "app-nextcloud"
  service-port = "8080"
  domain       = "nextcloud.service.wtbt.uk"
  namespace    = module.app-nextcloud-namespace.name

  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk"
  ]
}

module "app-nextcloud-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-nextcloud-db
  namespace         = module.app-nextcloud-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}
