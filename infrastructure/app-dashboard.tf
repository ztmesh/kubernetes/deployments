module "app-dashboard-namespace" {
  source = "./modules/namespace"
  name = "app-dashboard"
}

resource "helm_release" "app-dashboard" {
  name       = "app-dashboard"
  repository = "https://kubernetes.github.io/dashboard/"
  chart      = "kubernetes-dashboard"
  namespace  = module.app-dashboard-namespace.name

  values = ["${file("data/app-dashboard/values.yaml")}"]
}

module "app-dashboard-ingress" {
  source = "./modules/ingress"

  service-name = "app-dashboard-kong-proxy"
  service-port = "443"
  domain       = "k8s.service.wtbt.uk"
  insecure_transport = true
  namespace    = module.app-dashboard-namespace.name
  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk",
    "darius@identity.wtbt.uk"
  ]
}