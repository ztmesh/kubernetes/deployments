terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.26.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.12.1"
    }

    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.21.1-beta.1"
    }
  }

  backend "s3" {
    bucket  = "g8-terraform-state"
    key     = "k8s-deployments/terraform.tfstate"
    region  = "eu-west-2"
    encrypt = false
    profile = "jw-personal"
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "k3s-ansible"
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "k3s-ansible"
  }
}

provider "postgresql" {
  host            = var.postgres_server_host
  port            = 5432
  username        = var.postgres_server_username
  password        = var.postgres_server_password
  sslmode         = "disable"
  connect_timeout = 15
}

provider "aws" {
  region  = var.region
  profile = var.profile

  default_tags {
    tags = {
      service = "k8s-cluster"
    }
  }
}