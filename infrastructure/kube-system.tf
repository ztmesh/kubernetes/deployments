# Prometheus service account
resource "kubernetes_cluster_role_v1" "kube-system-prometheus" {
  metadata {
    name = "ext-prometheus"
  }

  rule {
    api_groups = [""]
    resources  = ["nodes", "nodes/metrics", "services/proxy", "nodes/proxy", "services", "endpoints", "pods", "pods/proxy"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    non_resource_urls = ["/metrics", "/metrics/cadvisor"]
    verbs             = ["get"]
  }
}

resource "kubernetes_service_account_v1" "kube-system-prometheus" {
  metadata {
    name      = "ext-prometheus"
    namespace = "kube-system"
  }
}

resource "kubernetes_secret_v1" "kube-system-prometheus" {
  metadata {
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.kube-system-prometheus.metadata.0.name
    }

    name      = "ext-prometheus"
    namespace = "kube-system"
  }

  type                           = "kubernetes.io/service-account-token"
  wait_for_service_account_token = true
}

resource "kubernetes_cluster_role_binding_v1" "kube-system-prometheus" {
  metadata {
    name = "ext-prometheus"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.kube-system-prometheus.metadata.0.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.kube-system-prometheus.metadata[0].name
    namespace = "kube-system"
  }
}

output "prometheus-sa-token" {
  value     = kubernetes_secret_v1.kube-system-prometheus.data.token
  sensitive = true
}
module "kube-system-etcd-backup" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-backup-cronjob.git?ref=v1.1.0"

  name                 = "k3s-etcd"
  namespace            = "kube-system"
  rsync_private_key    = var.rsync_private_key
  cron                 = "30 13 * * *"
  rsync_username       = var.rsync_username
  source_configuration = <<EOT
driver=ssh-tar
identity-file=/id_rsa_backup
host=${var.kube_node_ip}
username=root
command=cat /var/lib/rancher/k3s/server/db/snapshots/$(ls -t /var/lib/rancher/k3s/server/db/snapshots/ | head -n 1)
EOT
  extra_files = [{
    name       = "id_rsa_backup"
    mount_path = "/id_rsa_backup"
    content    = var.kube_node_private_key
  }]
}