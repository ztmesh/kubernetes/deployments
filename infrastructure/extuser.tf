resource "kubernetes_service_account_v1" "darius" {
  metadata {
    name      = "darius"
    namespace = module.darius-namespace.name
  }
}

resource "kubernetes_secret_v1" "darius" {
  metadata {
    name      = "darius-token"
    namespace = module.darius-namespace.name

    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.darius.metadata.0.name
    }
  }

  type                           = "kubernetes.io/service-account-token"
  wait_for_service_account_token = true
}

module "darius-namespace" {
  source = "./modules/namespace"
  name = "darius"
}

resource "kubernetes_role_v1" "darius" {
  metadata {
    name      = "darius"
    namespace = module.darius-namespace.name
  }

  rule {
    api_groups     = ["", "extensions", "apps"]
    resources      = ["*"]
    resource_names = ["*"]

    verbs = ["*"]
  }
  rule {
    api_groups     = ["batch"]
    resources      = ["jobs", "cronjobs"]
    resource_names = ["*"]

    verbs = ["*"]
  }
}

resource "kubernetes_role_binding_v1" "darius" {
  metadata {
    name      = "darius"
    namespace = module.darius-namespace.name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role_v1.darius.metadata[0].name
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.darius.metadata[0].name
  }
}

resource "kubernetes_cluster_role_v1" "darius" {
  metadata {
    name = "darius"
  }

  rule {
    api_groups = [""]
    resources  = ["namespaces"]

    verbs = ["get", "list"]
  }
}

resource "kubernetes_cluster_role_binding_v1" "darius" {
  metadata {
    name = "darius"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.darius.metadata[0].name
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.darius.metadata[0].name
    namespace = module.darius-namespace.name
  }
}
