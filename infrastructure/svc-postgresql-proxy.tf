module "svc-postgresql-proxy-namespace" {
  source = "./modules/namespace"
  name = "svc-postgresql-proxy"
}
resource "helm_release" "svc-postgresql-proxy" {
  name       = "haproxy-postgresql"
  repository = "https://haproxytech.github.io/helm-charts"
  chart      = "haproxy"
  namespace  = module.svc-postgresql-proxy-namespace.name

  values = ["${file("data/svc-postgresql-proxy/values.yaml")}"]
}

resource "kubernetes_network_policy_v1" "svc-postgresql-proxy-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-postgresql-proxy-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "9153"
        protocol = "TCP"
      }
    }
  }
}

resource "kubernetes_network_policy_v1" "svc-postgresql-proxy-ingress" {
  metadata {
    name      = "allow-svclb-ingress"
    namespace = module.svc-postgresql-proxy-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = "0.0.0.0/0"
        }
      }
      ports {
        port     = "5432"
        protocol = "TCP"
      }
    }
  }
}