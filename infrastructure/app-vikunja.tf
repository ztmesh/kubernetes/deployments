module "app-vikunja-namespace" {
  source = "./modules/namespace"
  name = "app-vikunja"
}

resource "random_password" "app-vikunja-jwt-secret" {
  length           = 64
  special          = true
  override_special = "!#%&*()-_=+[]<>:?"
}

module "app-vikunja-db" {
  source = "./modules/postgres"

  name = "vikunja"
}

resource "helm_release" "app-vikunja" {
  name       = "app-vikunja"
  repository = "https://kolaente.dev/api/packages/vikunja/helm"
  chart      = "vikunja"
  namespace  = module.app-vikunja-namespace.name

  values = ["${templatefile("data/app-vikunja/values.yaml", {
    database_password = module.app-vikunja-db.password
    jwt_secret        = random_password.app-vikunja-jwt-secret.result
  })}"]
}

module "app-vikunja-ingress" {
  source = "./modules/ingress"

  service-name = "vikunja"
  domain       = "tasks.service.wtbt.uk"
  namespace    = module.app-vikunja-namespace.name

  routes = [
    {
      match = "Host(`tasks.service.wtbt.uk`) && PathPrefix(`/api/v1/`)"
      kind  = "Rule"
      services = [
        {
          name      = "app-vikunja-api"
          namespace = module.app-vikunja-namespace.name
          port      = "http"
          kind      = "Service"
        }
      ]
    },
    {
      match = "Host(`tasks.service.wtbt.uk`)"
      kind  = "Rule"
      services = [
        {
          name      = "app-vikunja-frontend"
          namespace = module.app-vikunja-namespace.name
          port      = "http"
          kind      = "Service"
        }
      ]
    }
  ]
}

module "app-vikunja-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-vikunja-db
  namespace         = module.app-vikunja-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}

resource "kubernetes_network_policy_v1" "app-vikunja-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.app-vikunja-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "3456"
        protocol = "TCP"
      }
    }
  }
}