module "app-tss-namespace" {
  source = "./modules/namespace"
  name = "app-tss"
}

resource "kubernetes_service_account_v1" "app-tss-k8s-admin" {
  metadata {
    name = "tss-k8s-admin"
    namespace = module.app-tss-namespace.name
  }
}


resource "kubernetes_cluster_role_binding_v1" "app-tss-k8s-admin" {
  metadata {
    name = "tss-k8s-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.app-tss-k8s-admin.metadata[0].name
    namespace = module.app-tss-namespace.name
  }
}

resource "kubernetes_secret_v1" "app-tss-k8s-admin" {
  metadata {
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.app-tss-k8s-admin.metadata[0].name
      "kubernetes.io/service-account.namespace" = kubernetes_service_account_v1.app-tss-k8s-admin.metadata[0].namespace
    }

    generate_name = "tss-k8s-admin-"
    namespace = module.app-tss-namespace.name
  }

  type                           = "kubernetes.io/service-account-token"
  wait_for_service_account_token = true
}

output "app-tss-k8s-token" {
  value = kubernetes_secret_v1.app-tss-k8s-admin.data
  sensitive = true
}

module "app-tss-db" {
  source = "./modules/postgres"

  name = "tss"
}

module "app-tss-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-tss-db
  namespace         = module.app-tss-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}

resource "kubernetes_config_map_v1" "app-tss" {
  metadata {
    name      = "tss"
    namespace = module.app-tss-namespace.name
  }

  data = {
    "settings.py" = "TORTOISE_ORM = ${jsonencode({
      connections = {
        default = "postgres://${module.app-tss-db.username}:${module.app-tss-db.password}@${module.app-tss-db.host}:5432/${module.app-tss-db.database}"
      },
      apps = {
        models = {
          models = ["models", "aerich.models"],
          default_connection = "default"
        }
      }
    })}"
  }
}


resource "kubernetes_deployment_v1" "app-tss" {
  metadata {
    name      = "tss"
    namespace = module.app-tss-namespace.name

    labels = {
      "kubernetes.io/name" = "tss"
    }
  }

  spec {
    revision_history_limit = 0
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_unavailable = 1
      }
    }

    selector {
      match_labels = {
      "kubernetes.io/name" = "tss"
      }
    }

    template {
      metadata {
        labels = {
      "kubernetes.io/name" = "tss"
        }
      }

      spec {
        container {
          name              = "homehub"
          image             = "registry.gitlab.com/codingjwilliams/terraform-self-service:main"
          image_pull_policy = "Always"
          resources {
            limits = {
              memory = "170Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "70Mi"
            }
          }

          volume_mount {
            name       = "config"
            mount_path = "/app/settings.py"
            sub_path   = "settings.py"
          }

          volume_mount {
            name       = "ca"
            mount_path = "/ca.crt"
            sub_path   = "ca.crt"
          }

          port {
            container_port = 8000
            name           = "http"
            protocol       = "TCP"
          }
        }

        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map_v1.app-tss.metadata[0].name
          }
        }

        volume {
          name = "ca"
          secret {
            secret_name = "wtbt-root-ca"
            items {
              key  = "ca.crt"
              path = "ca.crt"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "app-tss" {
  metadata {
    name      = "tss"
    namespace = module.app-tss-namespace.name
    labels = {
      "kubernetes.io/name" = "tss"
    }
  }

  spec {
    selector = {
      "kubernetes.io/name" = "tss"
    }

    port {
      name        = "http"
      port        = "80"
      target_port = "http"
      protocol    = "TCP"
    }
    ip_family_policy = "SingleStack"
    type             = "ClusterIP"
    session_affinity = "None"
    ip_families      = ["IPv4"]
  }
}

module "app-tss-ingress" {
  source = "./modules/ingress"

  service-name = kubernetes_service_v1.app-tss.metadata[0].name
  service-port = "http"
  domain       = "tss.service.wtbt.uk"
  namespace    = module.app-tss-namespace.name

  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk",
    "darius@identity.wtbt.uk",
  ]
}

