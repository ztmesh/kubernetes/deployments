module "svc-longhorn-namespace" {
  source = "./modules/namespace"
  name = "svc-longhorn"
}

resource "helm_release" "svc-longhorn" {
  name       = "svc-longhorn"
  repository = "https://charts.longhorn.io"
  chart      = "longhorn"
  namespace  = module.svc-longhorn-namespace.name

  values = ["${file("data/svc-longhorn/values.yaml")}"]
}

module "svc-longhorn-ingress" {
  source = "./modules/ingress"

  service-name = "longhorn-frontend"
  domain       = "longhorn.service.wtbt.uk"
  namespace    = module.svc-longhorn-namespace.name
  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk"
  ]
}