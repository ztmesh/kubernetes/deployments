output "host" {
  value = "email-smtp.${data.aws_region.region.name}.amazonaws.com"
}

output "from" {
  value = local.sending_address
}

output "from_name" {
  value = var.from_name
}

output "username" {
  value = aws_iam_access_key.mail.id
}

output "password" {
  value = aws_iam_access_key.mail.ses_smtp_password_v4
}

data "aws_region" "region" {
}