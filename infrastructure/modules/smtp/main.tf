locals {
  sending_address = "${var.from}@${var.email_domain}"
}

data "aws_iam_policy_document" "mail" {
  statement {
    actions   = ["ses:SendRawEmail"]
    effect    = "Allow"
    resources = [data.aws_ses_domain_identity.mail.arn]

    condition {
      test     = "StringEquals"
      variable = "ses:FromAddress"
      values   = [local.sending_address]
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromDisplayName"
      values   = [var.from_name]
    }
  }
}

resource "aws_iam_policy" "mail" {
  name        = "g8-k8s-prod-smtp-${var.from}"
  description = "Policy to allow the K8S service to send outgoing mail"

  policy = data.aws_iam_policy_document.mail.json
}

resource "aws_iam_user" "mail" {
  name = "g8-k8s-prod-smtp-${var.from}"
}

resource "aws_iam_user_policy_attachment" "mail" {
  user       = aws_iam_user.mail.name
  policy_arn = aws_iam_policy.mail.arn
}

resource "aws_iam_access_key" "mail" {
  user = aws_iam_user.mail.name
}

data "aws_ses_domain_identity" "mail" {
  domain = var.email_domain
}

data "aws_iam_policy_document" "mailSes" {
  statement {
    actions   = ["SES:SendRawEmail"]
    resources = [data.aws_ses_domain_identity.mail.arn]

    principals {
      identifiers = [aws_iam_user.mail.arn]
      type        = "AWS"
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromAddress"
      values   = [local.sending_address]
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromDisplayName"
      values   = [var.from_name]
    }
  }
}

resource "aws_ses_identity_policy" "mail" {
  identity = data.aws_ses_domain_identity.mail.arn
  name     = "g8-k8s-prod-smtp-${var.from}"
  policy   = data.aws_iam_policy_document.mailSes.json
  depends_on = [ aws_iam_user.mail ]
}
