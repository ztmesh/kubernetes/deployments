variable "from" {
  type        = string
  description = "Name of the email account (e.g. 'bitwarden')"
}

variable "from_name" {
  type        = string
  description = "Display name of the email account (e.g. 'Bitwarden Server')"
}

variable "email_domain" {
  type        = string
  description = "Email domain to use. Must already be registered with AWS SES"
}