resource "postgresql_role" "role" {
  name     = var.name
  login    = true
  password = random_password.password.result
}

resource "random_password" "password" {
  length  = 24
  special = var.password_special_characters
}

resource "postgresql_database" "db" {
  name              = var.name
  owner             = postgresql_role.role.name
  connection_limit  = -1
  allow_connections = true
}

resource "postgresql_grant" "read_insert_column" {
  database    = postgresql_database.db.name
  role        = postgresql_role.role.name
  schema      = "public"
  object_type = "database"
  privileges  = ["CONNECT", "CREATE", "TEMPORARY"]
  #   privileges  = ["SELECT", "UPDATE", "INSERT", "DELETE", "TRUNCATE", "CREATE", "CONNECT", "EXECUTE", "USAGE"]
}

terraform {
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.21.1-beta.1"
    }
  }
}
