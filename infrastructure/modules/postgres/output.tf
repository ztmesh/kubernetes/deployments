output "password" {
    value = random_password.password.result
}
output "username" {
    value = var.name
}
output "host" {
    value = "postgres.service.wtbt.uk"
}
output "database" {
    value = var.name
}