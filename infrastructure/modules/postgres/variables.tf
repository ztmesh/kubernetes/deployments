variable "name" {
  type = string
  description = "Name of the database and user to create"
}

variable "password_special_characters" {
  type = bool
  description = "Enable special characters in the password"
  default = true
}