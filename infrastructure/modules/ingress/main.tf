resource "kubernetes_manifest" "certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"
    metadata = {
      name      = "tls-${var.domain}"
      namespace = var.namespace
    }
    spec = {
      secretName  = "tls-${var.domain}"
      duration    = "2160h"
      renewBefore = "360h"
      commonName  = var.domain
      isCA        = false
      privateKey = {
        algorithm = "RSA"
        encoding  = "PKCS1"
        size      = 2048
      }
      usages = [
        "server auth",
        "client auth"
      ]
      dnsNames = [
        var.domain
      ]
      issuerRef = {
        name = "cluster-ca"
        kind = "ClusterIssuer"
      }
    }
  }
}

resource "kubernetes_manifest" "middleware-cert" {
  count = length(var.restrict_certificate_subjects) > 0 ? 1 : 0
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "${sha256(var.domain)}-middleware-cert"
      namespace = var.namespace
    }

    spec = {
      passTLSClientCert = {
        pem = true
        info = {
          subject = {
            commonName = true
          }
        }
      }
    }
  }
}

resource "kubernetes_manifest" "middleware-allowedsubjects" {
  count = length(var.restrict_certificate_subjects) > 0 ? 1 : 0
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "${sha256(var.domain)}-middleware-allowedsubjects"
      namespace = var.namespace
    }

    spec = {
      headers = {
        customRequestHeaders = {
          "X-Forwarded-Allowed-Subjects" = join(",", var.restrict_certificate_subjects)
        }
      }
    }
  }
}

resource "kubernetes_manifest" "middleware-auth" {
  count = length(var.restrict_certificate_subjects) > 0 ? 1 : 0
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "${sha256(var.domain)}-middleware-auth"
      namespace = var.namespace
    }

    spec = {
      forwardAuth = {
        address = "http://127.0.0.1:8071/verify"
      }
    }
  }
}

resource "kubernetes_manifest" "middleware-chain" {
  count = length(var.restrict_certificate_subjects) > 0 ? 1 : 0
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "${sha256(var.domain)}-middleware-chain"
      namespace = var.namespace
    }

    spec = {
      chain = {
        middlewares = [
          {
            name = kubernetes_manifest.middleware-cert[0].manifest.metadata.name
          },
          {
            name = kubernetes_manifest.middleware-allowedsubjects[0].manifest.metadata.name
          },
          {
            name = kubernetes_manifest.middleware-auth[0].manifest.metadata.name
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "server_transport" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "ServersTransport"
    metadata = {
      name      = "ingress-${var.domain}-transport"
      namespace = var.namespace
    }

    spec = {
      insecureSkipVerify = var.insecure_transport
    }
  }
}
resource "kubernetes_manifest" "ingressroute" {
  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"
    metadata = {
      name      = "ingress-${var.domain}"
      namespace = var.namespace
    }
    spec = {
      entryPoints = ["web", "websecure"]
      routes = try(coalescelist(var.routes, [
        {
          match = "Host(`${var.domain}`)"
          kind  = "Rule"
          services = [
            {
              name      = var.service-name
              namespace = var.namespace
              serversTransport = kubernetes_manifest.server_transport.manifest.metadata.name
              port      = var.service-port
              kind      = "Service"
            }
          ]
          middlewares = length(var.restrict_certificate_subjects) > 0 ? [
            {
              name = kubernetes_manifest.middleware-chain[0].manifest.metadata.name
          }] : []
      }], []))
      tls = {
        secretName = "tls-${var.domain}"
        options = {
          name      = "user-ca"
          namespace = "svc-ingress"
        }
        domains = [
          {
            main = var.domain
          }
        ]
      }
    }
  }
}

resource "kubernetes_network_policy_v1" "traefik-ingress-policy" {
  metadata {
    name = "traefik-ingress-policy"
    namespace = var.namespace
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        namespace_selector {
          match_labels = {
            namespace = "svc-ingress"
          }
        }
      }
    }
  }
}