variable "domain" {
  type        = string
  description = "Domain to use for certificates & ingress rules"
}

variable "namespace" {
  type        = string
  description = "Namespace in which to create the certificates & ingress routes"
}

variable "service-name" {
  type        = string
  description = "Kubernetes service to send ingress traffic to"
}

variable "service-port" {
  type        = string
  description = "Port on service"
  default     = "http"
}

variable "routes" {
  type        = any
  description = "Custom rules for the ingress route"
  default     = []
}

variable "insecure_transport" {
  type        = bool
  description = "Disable HTTPS checks for backends"
  default     = false
}

variable "restrict_certificate_subjects" {
  type = list(string)
  description = "Certificate subject names to restrict access to"
  default = []
}