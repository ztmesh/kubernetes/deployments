resource "kubernetes_namespace_v1" "namespace" {
  metadata {
    name = var.name

    labels = {
      namespace = var.name
    }
  }
}

resource "kubernetes_network_policy_v1" "default-ingress" {
  metadata {
    name = "default-ingress"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        namespace_selector {
          match_labels = {
            namespace = var.name
          }
        }
      }
    }
  }
}