module "svc-dns-int-namespace" {
  source = "./modules/namespace"
  name = "svc-dns-int"
}

resource "kubernetes_network_policy_v1" "svc-dns-int-ingress" {
  metadata {
    name = "allow-dns-ingress"
    namespace = module.svc-dns-int-namespace.name
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = "0.0.0.0/0"
        }
      }
      ports {
        port = "53"
        protocol = "UDP"
      }
    }
  }
}

resource "kubernetes_service_account_v1" "svc-dns-int-sa" {
  metadata {
    name      = "coredns"
    namespace = module.svc-dns-int-namespace.name
  }
}

resource "kubernetes_cluster_role_v1" "svc-dns-int-crole" {
  metadata {
    name = "system:coredns"
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints", "services", "namespaces", "pods"]
    verbs      = ["list", "watch"]
  }

  rule {
    api_groups = ["discovery.k8s.io"]
    resources  = ["endpointslices"]
    verbs      = ["list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding_v1" "svc-dns-int-crb" {
  metadata {
    name = "coredns"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.svc-dns-int-crole.id
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.svc-dns-int-sa.metadata[0].name
    namespace = module.svc-dns-int-namespace.name
  }
}

resource "kubernetes_config_map_v1" "svc-dns-int-configmap" {
  metadata {
    name      = "coredns"
    namespace = module.svc-dns-int-namespace.name
  }

  data = {
    Corefile  = "${file("data/svc-dns-int/Corefile")}"
    NodeHosts = "${file("data/svc-dns-int/NodeHosts")}"
  }
}

resource "kubernetes_manifest" "svc-dns-int-deployment" {
  manifest = {
    "apiVersion" = "apps/v1"
    "kind"       = "Deployment"
    "metadata" = {
      "labels" = {
        "k8s-app"            = "kube-dns"
        "kubernetes.io/name" = "CoreDNS"
      }
      "name"      = "coredns"
      "namespace" = "svc-dns-int"
    }
    "spec" = {
      "revisionHistoryLimit" = 0
      "selector" = {
        "matchLabels" = {
          "k8s-app" = "kube-dns"
        }
      }
      "strategy" = {
        "rollingUpdate" = {
          "maxUnavailable" = 1
        }
        "type" = "RollingUpdate"
      }
      "template" = {
        "metadata" = {
          "labels" = {
            "k8s-app" = "kube-dns"
          }
        }
        "spec" = {
          "containers" = [
            {
              "args" = [
                "-conf",
                "/etc/coredns/Corefile",
              ]
              "image"           = "rancher/mirrored-coredns-coredns:1.10.1"
              "imagePullPolicy" = "IfNotPresent"
              "livenessProbe" = {
                "failureThreshold" = 3
                "httpGet" = {
                  "path"   = "/health"
                  "port"   = 8080
                  "scheme" = "HTTP"
                }
                "initialDelaySeconds" = 60
                "periodSeconds"       = 10
                "successThreshold"    = 1
                "timeoutSeconds"      = 1
              }
              "name" = "coredns"
              "ports" = [
                {
                  "containerPort" = 53
                  "name"          = "dns"
                  "protocol"      = "UDP"
                },
                {
                  "containerPort" = 53
                  "name"          = "dns-tcp"
                  "protocol"      = "TCP"
                },
                {
                  "containerPort" = 9153
                  "name"          = "metrics"
                  "protocol"      = "TCP"
                },
              ]
              "readinessProbe" = {
                "failureThreshold" = 3
                "httpGet" = {
                  "path"   = "/ready"
                  "port"   = 8181
                  "scheme" = "HTTP"
                }
                # "initialDelaySeconds" = null
                "periodSeconds"    = 2
                "successThreshold" = 1
                "timeoutSeconds"   = 1
              }
              "resources" = {
                "limits" = {
                  "memory" = "170Mi"
                }
                "requests" = {
                  "cpu"    = "100m"
                  "memory" = "70Mi"
                }
              }
              "securityContext" = {
                "allowPrivilegeEscalation" = false
                "capabilities" = {
                  "add" = [
                    "NET_BIND_SERVICE",
                  ]
                  "drop" = [
                    "all",
                  ]
                }
                "readOnlyRootFilesystem" = true
              }
              "volumeMounts" = [
                {
                  "mountPath" = "/etc/coredns-local"
                  "name"      = "config-volume"
                  "readOnly"  = true
                },
                {
                  "mountPath" = "/etc/coredns/custom"
                  "name"      = "custom-config-volume"
                  "readOnly"  = true
                },
                {
                  "mountPath" = "/etc/coredns"
                  "name"      = "cache-volume"
                },
              ]
            },
          ]
          "dnsPolicy" = "Default"
          "initContainers" = [
            {
              "command" = [
                "sh",
                "-c",
                "sed \"s/__MY_IP__/$HOST_IP/g\" /etc/coredns-local/Corefile > /etc/coredns/Corefile",
              ]
              "env" = [
                {
                  "name" = "HOST_IP"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "status.hostIP"
                    }
                  }
                },
                {
                  "name" = "NODENAME"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "spec.nodeName"
                    }
                  }
                },
              ]
              "image" = "busybox:1.28"
              "name"  = "init-coredns"
              "volumeMounts" = [
                {
                  "mountPath" = "/etc/coredns-local"
                  "name"      = "config-volume"
                  "readOnly"  = true
                },
                {
                  "mountPath" = "/etc/coredns"
                  "name"      = "cache-volume"
                },
              ]
            },
          ]
          "nodeSelector" = {
            "kubernetes.io/os" = "linux"
          }
          "priorityClassName"  = "system-cluster-critical"
          "serviceAccountName" = "coredns"
          "tolerations" = [
            {
              "key"      = "CriticalAddonsOnly"
              "operator" = "Exists"
            },
            {
              "effect"   = "NoSchedule"
              "key"      = "node-role.kubernetes.io/control-plane"
              "operator" = "Exists"
            },
            {
              "effect"   = "NoSchedule"
              "key"      = "node-role.kubernetes.io/master"
              "operator" = "Exists"
            },
          ]
          "topologySpreadConstraints" = [
            {
              "labelSelector" = {
                "matchLabels" = {
                  "k8s-app" = "kube-dns"
                }
              }
              "maxSkew"           = 1
              "topologyKey"       = "kubernetes.io/hostname"
              "whenUnsatisfiable" = "DoNotSchedule"
            },
          ]
          "volumes" = [
            {
              "configMap" = {
                "items" = [
                  {
                    "key"  = "Corefile"
                    "path" = "Corefile"
                  },
                  {
                    "key"  = "NodeHosts"
                    "path" = "NodeHosts"
                  },
                ]
                "name" = "coredns"
              }
              "name" = "config-volume"
            },
            {
              "configMap" = {
                "name"     = "coredns-custom"
                "optional" = true
              }
              "name" = "custom-config-volume"
            },
            {
              "emptyDir" = {}
              "name"     = "cache-volume"
            },
          ]
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "svc-dns-int-service" {
  metadata {
    name      = "kube-dns"
    namespace = module.svc-dns-int-namespace.name

    annotations = {
      "prometheus.io/port"   = "9153"
      "prometheus.io/scrape" = "true"
    }

    labels = {
      "k8s-app"                       = "kube-dns"
      "kubernetes.io/cluster-service" = "true"
      "kubernetes.io/name"            = "CoreDNS"
    }


  }
  spec {
    cluster_ip = "10.43.0.10"
    cluster_ips = [
      "10.43.0.10",
    ]
    ip_family_policy = "SingleStack"
    port {
      name     = "dns"
      port     = 53
      protocol = "UDP"
    }
    port {
      name     = "dns-tcp"
      port     = 53
      protocol = "TCP"
    }
    port {
      name     = "metrics"
      port     = 9153
      protocol = "TCP"
    }

    selector = {
      k8s-app = "kube-dns"
    }
  }
}