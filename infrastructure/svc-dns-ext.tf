module "svc-dns-ext-namespace" {
  source = "./modules/namespace"
  name = "svc-dns-ext"
}

resource "kubernetes_config_map_v1" "svc-dns-ext-configmap" {
  metadata {
    name      = "dns-ext"
    namespace = module.svc-dns-ext-namespace.name
  }

  data = {
    Corefile              = "${file("data/svc-dns-ext/Corefile")}"
    "service.wtbt.uk.tpl" = "${file("data/svc-dns-ext/service.wtbt.uk.tpl")}"
  }
}

resource "kubernetes_network_policy_v1" "svc-dns-ext-ingress" {
  metadata {
    name      = "allow-svclb-ingress"
    namespace = module.svc-dns-ext-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = "0.0.0.0/0"
        }
      }
      ports {
        port     = "53"
        protocol = "UDP"
      }
    }
  }
}

resource "kubernetes_manifest" "svc-dns-ext-deployment" {
  manifest = {
    "apiVersion" = "apps/v1"
    "kind"       = "DaemonSet"
    "metadata" = {
      "labels" = {
        "k8s-app"            = "dns-ext"
        "kubernetes.io/name" = "dns-ext"
      }
      "name"      = "dns-ext"
      "namespace" = "svc-dns-ext"
    }
    "spec" = {
      "revisionHistoryLimit" = 0
      "selector" = {
        "matchLabels" = {
          "k8s-app" = "dns-ext"
        }
      }
      "template" = {
        "metadata" = {
          "labels" = {
            "k8s-app" = "dns-ext"
          }
        }
        "spec" = {
          "containers" = [
            {
              "args" = [
                "-conf",
                "/etc/coredns/Corefile",
              ]
              "env" = [
                {
                  "name" = "HOST_IP"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "status.hostIP"
                    }
                  }
                },
                {
                  "name" = "HOST_IPLOL"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "metadata.name"
                    }
                  }
                },
                {
                  "name" = "NODENAME"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "spec.nodeName"
                    }
                  }
                },
              ]
              "image"           = "rancher/mirrored-coredns-coredns:1.10.1"
              "imagePullPolicy" = "IfNotPresent"
              "livenessProbe" = {
                "failureThreshold" = 3
                "httpGet" = {
                  "path"   = "/health"
                  "port"   = 8080
                  "scheme" = "HTTP"
                }
                "initialDelaySeconds" = 60
                "periodSeconds"       = 10
                "successThreshold"    = 1
                "timeoutSeconds"      = 1
              }
              "name" = "coredns"
              "ports" = [
                {
                  "containerPort" = 53
                  "name"          = "dns"
                  "protocol"      = "UDP"
                },
                {
                  "containerPort" = 53
                  "name"          = "dns-tcp"
                  "protocol"      = "TCP"
                },
                {
                  "containerPort" = 9153
                  "name"          = "metrics"
                  "protocol"      = "TCP"
                },
              ]
              "readinessProbe" = {
                "failureThreshold" = 3
                "httpGet" = {
                  "path"   = "/ready"
                  "port"   = 8181
                  "scheme" = "HTTP"
                }
                # "initialDelaySeconds" = 0
                "periodSeconds"    = 2
                "successThreshold" = 1
                "timeoutSeconds"   = 1
              }
              "resources" = {
                "limits" = {
                  "memory" = "170Mi"
                }
                "requests" = {
                  "cpu"    = "100m"
                  "memory" = "70Mi"
                }
              }
              "securityContext" = {
                "allowPrivilegeEscalation" = false
                "capabilities" = {
                  "add" = [
                    "NET_BIND_SERVICE",
                  ]
                  "drop" = [
                    "all",
                  ]
                }
                "readOnlyRootFilesystem" = true
              }
              "volumeMounts" = [
                {
                  "mountPath" = "/etc/coredns"
                  "name"      = "config-volume"
                  "readOnly"  = true
                },
                {
                  "mountPath" = "/etc/zones"
                  "name"      = "zones"
                  "readOnly"  = true
                },
              ]
            },
          ]
          "dnsPolicy" = "Default"
          "initContainers" = [
            {
              "command" = [
                "sh",
                "-c",
                "sed \"s/__MY_IP__/$HOST_IP/g\" /etc/coredns/service.wtbt.uk.tpl > /etc/zones/service.wtbt.uk",
              ]
              "env" = [
                {
                  "name" = "HOST_IP"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "status.hostIP"
                    }
                  }
                },
                {
                  "name" = "NODENAME"
                  "valueFrom" = {
                    "fieldRef" = {
                      "fieldPath" = "spec.nodeName"
                    }
                  }
                },
              ]
              "image" = "busybox:1.28"
              "name"  = "init-coredns"
              "volumeMounts" = [
                {
                  "mountPath" = "/etc/coredns"
                  "name"      = "config-volume"
                  "readOnly"  = true
                },
                {
                  "mountPath" = "/etc/zones"
                  "name"      = "zones"
                },
              ]
            },
          ]
          "nodeSelector" = {
            "kubernetes.io/os" = "linux"
          }
          "priorityClassName" = "system-cluster-critical"
          "tolerations" = [
            {
              "key"      = "CriticalAddonsOnly"
              "operator" = "Exists"
            },
            {
              "effect"   = "NoSchedule"
              "key"      = "node-role.kubernetes.io/control-plane"
              "operator" = "Exists"
            },
            {
              "effect"   = "NoSchedule"
              "key"      = "node-role.kubernetes.io/master"
              "operator" = "Exists"
            },
          ]
          "topologySpreadConstraints" = [
            {
              "labelSelector" = {
                "matchLabels" = {
                  "k8s-app" = "dns-ext"
                }
              }
              "maxSkew"           = 1
              "topologyKey"       = "kubernetes.io/hostname"
              "whenUnsatisfiable" = "DoNotSchedule"
            },
          ]
          "volumes" = [
            {
              "emptyDir" = {}
              "name"     = "zones"
            },
            {
              "configMap" = {
                "items" = [
                  {
                    "key"  = "Corefile"
                    "path" = "Corefile"
                  },
                  {
                    "key"  = "service.wtbt.uk.tpl"
                    "path" = "service.wtbt.uk.tpl"
                  },
                ]
                "name" = "dns-ext"
              }
              "name" = "config-volume"
            },
          ]
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "svc-dns-ext-service" {
  metadata {
    name      = "dns-ext"
    namespace = module.svc-dns-ext-namespace.name

    # annotations = {
    #   "prometheus.io/port"   = "9153"
    #   "prometheus.io/scrape" = "true"
    # }

    labels = {
      "k8s-app"            = "dns-ext"
      "kubernetes.io/name" = "dns-ext"
    }
  }
  spec {
    external_traffic_policy = "Local"
    internal_traffic_policy = "Local"
    port {
      name     = "dns"
      port     = "53"
      protocol = "UDP"
    }

    port {
      name     = "dns-tcp"
      port     = "53"
      protocol = "TCP"
    }

    selector = {
      k8s-app = "dns-ext"
    }

    type = "LoadBalancer"
  }
}