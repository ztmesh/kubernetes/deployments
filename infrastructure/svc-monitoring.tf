module "svc-monitoring-namespace" {
  source = "./modules/namespace"
  name = "svc-monitoring"
}

resource "kubernetes_network_policy_v1" "svc-monitoring-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-monitoring-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "9187"
        protocol = "TCP"
      }
    }
  }
}

resource "kubernetes_deployment_v1" "svc-monitoring-postgres-exporter" {
  metadata {
    name      = "postgres-exporter"
    namespace = module.svc-monitoring-namespace.name
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "postgres-exporter"
      }
    }

    template {
      metadata {
        labels = {
          app = "postgres-exporter"
        }

        annotations = {
          "prometheus.io/scrape" = true
          "prometheus.io/port"   = 9187
          "prometheus.io/path"   = "/metrics"
        }
      }

      spec {
        container {
          image = "quay.io/prometheuscommunity/postgres-exporter"
          name  = "exporter"

          resources {
            requests = {
              cpu    = "50m"
              memory = "30Mi"
            }
          }

          env {
            name  = "DATA_SOURCE_NAME"
            value = "postgresql://${var.postgres_server_username}:${urlencode(var.postgres_server_password)}@${var.postgres_server_host}/postgres?sslmode=disable"
          }

          port {
            name           = "metrics"
            protocol       = "TCP"
            container_port = 9100
          }
        }
      }
    }
  }
}
