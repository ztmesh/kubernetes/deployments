$ORIGIN service.wtbt.uk.
@	3600 IN	SOA sns.dns.icann.org. noc.dns.icann.org. (
        2017042745 ; serial
        7200       ; refresh (2 hours)
        3600       ; retry (1 hour)
        1209600    ; expire (2 weeks)
        3600       ; minimum (1 hour)
        )

    3600 IN NS ns.service.wtbt.uk.

traefik-local IN A __MY_IP__

ns IN  A 10.6.0.1
* IN CNAME traefik-local
*.s3 IN CNAME traefik-local
prom-ip IN A 10.5.3.13
virtualphotos-staging IN A 10.5.1.2