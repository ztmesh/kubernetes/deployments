module "svc-loki-namespace" {
  source = "./modules/namespace"
  name = "svc-loki"
}

resource "helm_release" "svc-loki" {
  name       = "loki"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "loki"
  namespace  = module.svc-loki-namespace.name

  values = ["${templatefile("data/svc-loki/values.yaml", {
    loki_access_key_id     = var.loki_access_key_id
    loki_secret_access_key = var.loki_secret_access_key
  })}"]
}

# module "svc-loki" {
#   source = "./modules/ingress"

#   service-name = "loki-gateway"
#   domain       = "loki.service.wtbt.uk"
#   namespace    = module.svc-loki-namespace.name
# }


resource "kubernetes_config_map_v1" "svc-loki-promtail" {
  metadata {
    name      = "promtail-config"
    namespace = module.svc-loki-namespace.name
  }

  data = {
    "promtail.yaml" = "${file("data/svc-loki/promtail.yaml")}"
  }
}

resource "kubernetes_daemon_set_v1" "svc-loki-promtail" {
  metadata {
    name      = "promtail-daemonset"
    namespace = module.svc-loki-namespace.name
  }

  spec {
    selector {
      match_labels = {
        name = "promtail"
      }
    }

    template {
      metadata {
        labels = {
          name = "promtail"
        }
      }

      spec {
        service_account_name = "promtail-serviceaccount"
        container {
          name  = "promtail-container"
          image = "grafana/promtail"
          args = [
            "-config.file=/etc/promtail/promtail.yaml"
          ]

          env {
            name = "HOSTNAME"
            value_from {
              field_ref {
                field_path = "spec.nodeName"
              }
            }
          }

          volume_mount {
            name       = "logs"
            mount_path = "/var/log"
          }
          volume_mount {
            name       = "promtail-config"
            mount_path = "/etc/promtail"
          }
          volume_mount {
            name       = "varlibdockercontainers"
            mount_path = "/var/lib/docker/containers"
            read_only  = true

          }
          volume_mount {
            name       = "wtbt-root-ca"
            mount_path = "/certs"
            read_only  = true
          }
        }

        volume {
          name = "logs"
          host_path {
            path = "/var/log"
          }
        }

        volume {
          name = "varlibdockercontainers"
          host_path {
            path = "/var/lib/docker/containers"
          }
        }

        volume {
          name = "promtail-config"
          config_map {
            name = kubernetes_config_map_v1.svc-loki-promtail.metadata[0].name
          }
        }

        volume {
          name = "wtbt-root-ca"
          secret {
            secret_name = "wtbt-root-ca"
            items {
              key  = "ca.crt"
              path = "ca.crt"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_cluster_role_v1" "svc-loki-promtail" {
  metadata {
    name = "promtail-clusterrole"
  }

  rule {
    api_groups = [""]
    resources  = ["nodes", "services", "pods"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_service_account_v1" "svc-loki-promtail" {
  metadata {
    name      = "promtail-serviceaccount"
    namespace = module.svc-loki-namespace.name
  }
}

resource "kubernetes_cluster_role_binding_v1" "svc-loki-promtail" {
  metadata {
    name = "promtail-clusterrolebinding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.svc-loki-promtail.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.svc-loki-promtail.metadata[0].name
    namespace = module.svc-loki-namespace.name
  }
}

resource "kubernetes_network_policy_v1" "svc-loki-prometheus" {
  metadata {
    name = "prometheus"
    namespace = module.svc-loki-namespace.name
  }

  spec {
    pod_selector {}
    policy_types = ["Ingress"]
    ingress {
      from {
        ip_block {
          cidr = var.monitoring_host_cidr_block
        }
      }
      ports {
        port = "3500"
        protocol = "TCP"
      }
    }
  }
}