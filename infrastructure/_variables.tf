variable "postgres_server_host" {
  type        = string
  description = "Hostname / IP address of PostgreSQL server to connect to"
}

variable "postgres_server_username" {
  type        = string
  description = "Username of PostgreSQL server to connect to"
}

variable "postgres_server_password" {
  type        = string
  description = "Password of PostgreSQL server to connect to"
}

variable "cf_account_id" {
  type        = string
  description = "Account ID to deploy Cloudflare Ingress into"
}

variable "cf_tunnel_id" {
  type        = string
  description = "Tunnel ID to deploy Cloudflare Ingress into"
}

variable "cf_tunnel_secret" {
  type        = string
  description = "Tunnel Secret to deploy Cloudflare Ingress into"
}

variable "loki_access_key_id" {
  type = string
}

variable "loki_secret_access_key" {
  type = string
}

variable "profile" {
  description = "AWS profile"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "account_id" {
  description = "AWS account ID"
  type        = string
}

variable "ses_mail_domain" {
  type        = string
  description = "Email domain configured with Amazon SES"
}

variable "rsync_username" {
  type        = string
  description = "Example ab1234s3"
}

variable "rsync_private_key" {
  type        = string
  description = "Private key in OpenSSH format, with access to the backup user"
}

variable "gitlab_runner_tokens" {
  type        = list(string)
  description = "Gitlab Runner registration token"
}

variable "app_homehub_mysql_password" {
  type = string
}
variable "app_homehub_azure_client_id" {
  type = string
}
variable "app_homehub_azure_client_secret" {
  type = string
}
variable "app_homehub_s3_access_key_id" {
  type = string
}
variable "app_homehub_s3_secret_access_key" {
  type = string
}

variable "kube_node_ip" {
  type = string
}

variable "kube_node_private_key" {
  type = string
}

variable "monitoring_host_cidr_block" {
  type = string
}