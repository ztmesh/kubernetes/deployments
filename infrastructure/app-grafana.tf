module "app-grafana-namespace" {
  source = "./modules/namespace"
  name = "app-grafana"
}

module "app-grafana-db" {
  source = "./modules/postgres"

  name = "grafana"
  password_special_characters = false
}

resource "kubernetes_config_map_v1" "app-grafana" {
  metadata {
    name      = "grafana-config"
    namespace = module.app-grafana-namespace.name
  }

  data = {
    "grafana.ini" = templatefile("${path.root}/data/app-grafana/grafana.ini", {
      database = module.app-grafana-db
    })
  }
}

resource "kubernetes_persistent_volume_claim_v1" "app-grafana" {
  metadata {
    name      = "grafana-pvc"
    namespace = module.app-grafana-namespace.name
  }

  spec {
    storage_class_name = "longhorn"
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubernetes_deployment_v1" "app-grafana" {
  metadata {
    name      = "grafana"
    namespace = module.app-grafana-namespace.name

    labels = {
      app              = "grafana"
    }
  }

  spec {
    # revision_history_limit = 0
    # strategy {
    #   type = "RollingUpdate"
    #   rolling_update {
    #     max_unavailable = 1
    #   }
    # }

    selector {
      match_labels = {
        "app" = "grafana"
      }
    }

    template {
      metadata {
        labels = {
          "app" = "grafana"
        }
      }

      spec {
        security_context {
          fs_group = "472"
          supplemental_groups = [0]
        }

        container {
          name              = "grafana"
          image             = "grafana/grafana:latest"
          image_pull_policy = "IfNotPresent"

          port {
            container_port = 3000
            name = "http-grafana"
            protocol = "TCP"
          }

          readiness_probe {
            failure_threshold = 3
            http_get {
              path = "/robots.txt"
              port = 3000
              scheme = "HTTP"
            }

            initial_delay_seconds = 10
            period_seconds = 30
            success_threshold = 1
            timeout_seconds = 2
          }

          liveness_probe {
            failure_threshold = 3
            initial_delay_seconds = 30
            period_seconds = 10
            success_threshold = 1
            tcp_socket {
              port = 3000
            }
            timeout_seconds = 1
          }

          resources {
            requests = {
              cpu    = "250m"
              memory = "750Mi"
            }
          }

          volume_mount {
            mount_path = "/var/lib/grafana"
            name = "grafana-pv"
          }

          volume_mount {
            mount_path = "/etc/grafana"
            name = "grafana-config"
          }
        }

        volume {
          name = "grafana-pv"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.app-grafana.metadata[0].name
          }
        }

        volume {
          name = "grafana-config"
          config_map {
            name = kubernetes_config_map_v1.app-grafana.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "app-grafana" {
  metadata {
    name      = "grafana"
    namespace = module.app-grafana-namespace.name
  }

  spec {
    selector = {
      "app" = "grafana"
    }

    port {
      name     = "http"
      port     = "3000"
      protocol = "TCP"
      target_port = "http-grafana"
    }
    ip_family_policy = "SingleStack"
    type = "ClusterIP"
  }
}

module "app-grafana-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-grafana-db
  namespace         = module.app-grafana-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}

module "app-grafana-ingress" {
  source = "./modules/ingress"

  service-name = kubernetes_service_v1.app-grafana.metadata[0].name
  service-port = "http"
  domain       = "grafana.service.wtbt.uk"
  namespace    = module.app-grafana-namespace.name
  restrict_certificate_subjects = [
    "jay@identity.wtbt.uk"
  ]
}