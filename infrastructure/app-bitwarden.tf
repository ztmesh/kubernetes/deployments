module "app-bitwarden-namespace" {
  source = "./modules/namespace"
  name = "app-bitwarden"
}

module "app-bitwarden-smtp" {
  source = "./modules/smtp"

  from         = "bitwarden"
  from_name    = "Bitwarden"
  email_domain = var.ses_mail_domain
}

module "app-bitwarden-db" {
  source = "./modules/postgres"

  name = "bitwarden"
}

resource "random_password" "app-bitwarden-admin-token" {
  length = 128
}

resource "kubernetes_secret_v1" "app-bitwarden-admin-token" {
  metadata {
    name      = "bitwarden-tokens"
    namespace = module.app-bitwarden-namespace.name
  }

  data = {
    adminToken = random_password.app-bitwarden-admin-token.result
  }
}

resource "kubernetes_network_policy_v1" "app-bitwarden-cf-ingress" {
  metadata {
    name = "cloudflare-ingress"
    namespace = module.app-bitwarden-namespace.name
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        namespace_selector {
          match_labels = {
            namespace = module.svc-ingress-cloudflare-namespace.name
          }
        }
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "app-bitwarden" {
  metadata {
    name      = "bitwarden-data"
    namespace = module.app-bitwarden-namespace.name
  }

  spec {
    storage_class_name = "longhorn"
    access_modes       = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "3Gi"
      }
    }
  }
}

resource "kubernetes_deployment_v1" "app-bitwarden" {
  metadata {
    name      = "bitwarden"
    namespace = module.app-bitwarden-namespace.name

    labels = {
      k8s-app              = "bitwarden"
      "kubernetes.io/name" = "Bitwarden"
    }
  }

  spec {
    revision_history_limit = 0
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_unavailable = 1
      }
    }

    selector {
      match_labels = {
        "k8s-app" = "bitwarden"
      }
    }

    template {
      metadata {
        labels = {
          "k8s-app" = "bitwarden"
        }
      }

      spec {
        node_selector = {
          "topology.kubernetes.io/zone" = "a"
        }

        container {
          name              = "vaultwarden"
          image             = "vaultwarden/server:latest"
          image_pull_policy = "Always"
          resources {
            limits = {
              memory = "170Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "70Mi"
            }
          }

          volume_mount {
            name       = "bitwarden-data"
            mount_path = "/data"
          }

          env {
            name = "ADMIN_TOKEN"
            value_from {
              secret_key_ref {
                name = kubernetes_secret_v1.app-bitwarden-admin-token.metadata[0].name
                key  = "adminToken"
              }
            }
          }

          env {
            name  = "DATABASE_URL"
            value = "postgresql://${module.app-bitwarden-db.username}:${module.app-bitwarden-db.password}@${module.app-bitwarden-db.host}/${module.app-bitwarden-db.database}"
          }

          env {
            name  = "WEBSOCKET_ENABLED"
            value = "true"
          }

          env {
            name  = "DISABLE_ICON_DOWNLOAD"
            value = "true"
          }

          env {
            name  = "SIGNUPS_ALLOWED"
            value = "true"
          }

          env {
            name  = "INVITATION_ORG_NAME"
            value = "G8"
          }

          env {
            name  = "EXPERIMENTAL_CLIENT_FEATURE_FLAGS"
            value = "fido2-vault-credentials,autofill-v2,autofill-overlay"
          }

          env {
            name  = "IP_HEADER"
            value = "X-Forwarded-For"
          }

          env {
            name  = "DOMAIN"
            value = "https://bitwarden.g8.fyi"
          }

          env {
            name  = "SMTP_FROM"
            value = module.app-bitwarden-smtp.from
          }
          env {
            name  = "SMTP_FROM_NAME"
            value = module.app-bitwarden-smtp.from_name
          }
          env {
            name  = "SMTP_HOST"
            value = module.app-bitwarden-smtp.host
          }
          env {
            name  = "SMTP_USERNAME"
            value = module.app-bitwarden-smtp.username
          }
          env {
            name  = "SMTP_PASSWORD"
            value = module.app-bitwarden-smtp.password
          }
          env {
            name  = "ROCKET_PORT"
            value = "8000"
          }
          env {
            name  = "ROCKET_ADDRESS"
            value = "0.0.0.0"
          }

          port {
            container_port = 8000
            name           = "http"
            protocol       = "TCP"
          }
        }

        volume {
          name = "bitwarden-data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.app-bitwarden.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "app-bitwarden" {
  metadata {
    name      = "bitwarden"
    namespace = module.app-bitwarden-namespace.name
    labels = {
      "k8s-app"            = "bitwarden"
      "kubernetes.io/name" = "bitwarden"
    }
  }

  spec {
    selector = {
      "k8s-app" = "bitwarden"
    }

    port {
      name     = "http"
      port     = "8000"
      protocol = "TCP"
    }
    ip_family_policy = "SingleStack"
  }
}

module "app-bitwarden-db-backups" {
  source = "git::ssh://git@gitlab.com/ztmesh/terraform-modules/kubernetes-postgresql-backup-cronjob.git?ref=v1.0.0"

  database          = module.app-bitwarden-db
  namespace         = module.app-bitwarden-namespace.name
  rsync_private_key = var.rsync_private_key
  rsync_username    = var.rsync_username
}