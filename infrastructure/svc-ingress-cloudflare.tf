module "svc-ingress-cloudflare-namespace" {
  source = "./modules/namespace"
  name = "svc-ingress-cloudflare"
}

resource "helm_release" "svc-ingress-cloudflare" {
  name       = "cloudflare-tunnel"
  repository = "https://gitlab.com/api/v4/projects/55867860/packages/helm/stable"
  chart      = "cloudflare-tunnel"
  namespace  = module.svc-ingress-cloudflare-namespace.name

  values = ["${templatefile("data/svc-ingress-cloudflare/values.yaml", {
    credentials_secret = kubernetes_secret_v1.svc-ingress-cloudflare-tokens.metadata[0].name
  })}"]
}

resource "kubernetes_secret_v1" "svc-ingress-cloudflare-tokens" {
  metadata {
    name      = "cloudflare-tunnel-token"
    namespace = module.svc-ingress-cloudflare-namespace.name
  }

  data = {
    "credentials.json" = jsonencode({
      AccountTag   = var.cf_account_id
      TunnelID     = var.cf_tunnel_id
      TunnelSecret = var.cf_tunnel_secret
    })
  }
}