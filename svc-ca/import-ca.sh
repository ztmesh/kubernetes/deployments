scp ca01:/root/pki/wtbt/certs/k8s-cluster-ca/k8s-cluster-ca-fullchain.pem tls.crt
scp ca01:/root/pki/wtbt/certs/k8s-cluster-ca/k8s-cluster-ca-key.pem tls.key
scp ca01:/root/pki/wtbt/wtbt.pem ca.crt
kubectl delete secret -n svc-ca cluster-ca
kubectl create secret -n svc-ca generic cluster-ca \
    --from-file=tls.crt \
    --from-file=tls.key 


kubectl delete secret -n svc-ingress wtbt-root-ca
kubectl create secret -n svc-ingress generic wtbt-root-ca \
    --from-file=ca.crt 
kubectl annotate secret -n svc-ingress wtbt-root-ca --overwrite  reflector.v1.k8s.emberstack.com/reflection-allowed='true'
kubectl annotate secret -n svc-ingress wtbt-root-ca --overwrite  reflector.v1.k8s.emberstack.com/reflection-auto-enabled='true'

rm -rf tls.crt tls.key ca.crt