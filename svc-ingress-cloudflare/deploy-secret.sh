. ../secrets.env
kubectl delete secret -n svc-ingress-cloudflare cloudflare-tunnel-token
kubectl create secret -n svc-ingress-cloudflare generic cloudflare-tunnel-token --from-literal='credentials.json={
    "AccountTag": "'"$CLOUDFLARE_ACCOUNT"'", 
    "TunnelID": "'"$CLOUDFLARE_TUNNEL_ID"'", 
    "TunnelSecret": "'"$CLOUDFLARE_TUNNEL_SECRET"'"
}'