./garage.sh bucket create $1 2>&1
key="$(./garage.sh key create $1 2>&1)"
key_id="$(echo "$key" | grep 'Key ID' | cut -d' ' -f3)"
secret_key="$(echo "$key" | grep 'Secret key' | cut -d' ' -f3)"
sleep 1
./garage.sh bucket allow $1 --key "$key_id" --read --write
echo "------"
echo "BUCKET_NAME=$1"
echo "AWS_ACCESS_KEY_ID=$key_id"
echo "AWS_SECRET_ACCESS_KEY=$secret_key"
