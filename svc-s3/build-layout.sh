garage="kubectl exec --stdin --tty -n svc-s3 garage-0 -- ./garage"

for garage_pod in $($garage status | grep "10.42" | cut -d' ' -f3); do
    garage_pod_id="$($garage status | grep "$garage_pod" | grep "10.42" | cut -d' ' -f1)"

    node_name="$(kubectl get pod -n svc-s3 $garage_pod -o=custom-columns=NODE:.spec.nodeName | tail -n +2)"
    zone_name="$(kubectl get node $node_name -o=custom-columns=ZONE:".metadata.labels.topology\.kubernetes\.io/zone" | tail -n +2)"

    echo "layout assign -c 5G -z $zone_name $garage_pod_id ( $garage_pod )"
    $garage layout assign -c 5G -z $zone_name $garage_pod_id;
done;

apply_version="$($garage layout show | grep "version " | sed 's/.*--version //g' | sed 's/\t//g' | head -n 1 | sed 's/[^0-9]//g')"
echo "APplying ver:$apply_version |"
$garage layout apply --version $apply_version