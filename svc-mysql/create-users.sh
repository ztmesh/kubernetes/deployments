kubectl port-forward -n svc-mysql service/mysql 8089:3306 &
pf=$!
sleep 2

root_password="$(kubectl get -n svc-mysql secret/mysql-root-credentials --template="{{ .data.rootPassword }}" | base64 -d)"
mysqlexec="mysql -h 127.0.0.1 --port=8089 -u root -p$root_password -e"

# vikunja
vikunja_password="$(kubectl get -n app-vikunja secret/vikunja-mariadb --template="{{ .data.password }}" | base64 -d)"
$mysqlexec "CREATE USER IF NOT EXISTS 'vikunja'@'%' IDENTIFIED BY '$vikunja_password';"
$mysqlexec "CREATE DATABASE IF NOT EXISTS vikunja;"
$mysqlexec "GRANT ALL PRIVILEGES ON vikunja.* TO 'vikunja'@'%';"


# homehub
homehub_password="$(kubectl get -n app-homehub secret/homehub-mysql --template="{{ .data.password }}" | base64 -d)"
$mysqlexec "CREATE USER IF NOT EXISTS 'homehub'@'%' IDENTIFIED BY '$homehub_password';"
$mysqlexec "CREATE DATABASE IF NOT EXISTS homehub;"
$mysqlexec "GRANT ALL PRIVILEGES ON homehub.* TO 'homehub'@'%';"


kill $pf